#ifndef SSH_SHELL_HPP
#define SSH_SHELL_HPP

#include <libssh2.h>
#include <vector>
#include <string>
#include <cassert>
#include <boost/noncopyable.hpp>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/thread.hpp>
#include <boost/scope_exit.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>

namespace ssh {

class Shell;
typedef boost::shared_ptr<Shell> ShellPtr;
typedef std::vector<ShellPtr> Shells;

class Command;

class Shell: boost::noncopyable
{
 public:
  static ShellPtr create(
      boost::asio::io_service& ios
      , boost::filesystem::path const& ssh_dir
      , std::string const& user
      , std::string const& host
      , std::string const& port = "22"
  );
  boost::asio::io_service& get_io_service() { return m_socket.get_io_service(); }
  void reset();
  void disconnect();
  std::string host() const;
  std::string port() const;
  std::string release_reply();
  void log_verbosity(int v);
  void set_blocking(bool b = true);
  bool may_block() const;
  void async_connect(boost::asio::deadline_timer::duration_type const& timeout);
  void async_connect();
  bool is_connected() const;
  std::string public_key_hash() const;
  void async_exec(Command const& command, boost::asio::deadline_timer::duration_type const& timeout);
  void async_exec(Command const& command);

 private:
  Shell(
      boost::asio::io_service& ios
      , boost::filesystem::path const& ssh_dir
      , std::string const& user
      , std::string const& host
      , std::string const& port
  );
  void handle_connect(boost::system::error_code const& error);
  void try_connect(boost::system::error_code const& error);
  void try_ssh_handshake(boost::system::error_code const& error);
  void try_authenticate(boost::system::error_code const& error);
  void try_create_channel(boost::system::error_code const&);
  void try_create_shell(boost::system::error_code const&);
  void try_send_command(boost::system::error_code const& error);
  void try_read_reply(boost::system::error_code const& error);
  void on_timeout(boost::system::error_code const& error);

  template<typename Handler>
  void retry_when_ready(Handler handler)
  {
    int dir = libssh2_session_block_directions(m_session.get());
    // is the following true?
    assert(!((dir & LIBSSH2_SESSION_BLOCK_INBOUND) && (dir & LIBSSH2_SESSION_BLOCK_OUTBOUND)));
    if (dir & LIBSSH2_SESSION_BLOCK_INBOUND) {
      m_socket.async_read_some(boost::asio::null_buffers(), handler);
    }
    if (dir & LIBSSH2_SESSION_BLOCK_OUTBOUND) {
      m_socket.async_write_some(boost::asio::null_buffers(), handler);
    }
  }

 private:
  boost::filesystem::path m_ssh_dir;
  std::string m_user;
  std::string m_host;
  std::string m_port;

  boost::asio::ip::tcp::socket m_socket;
  boost::asio::deadline_timer m_timeout_timer;
  boost::asio::deadline_timer m_retry_timer;

  // internal buffer to manage the sending of a command and the receiving of a
  // reply; since the protocol is half-duplex the same buffer can be reused
  std::string m_buffer;
  // the iterator remembers where the send has arrived, in case of short writes
  std::string::iterator m_buffer_current;

  // character that determines the end-of-message in a reply
  // it is (re)set by each command
  char m_reply_delimiter;

  boost::shared_ptr<LIBSSH2_SESSION> m_session;
  boost::shared_ptr<LIBSSH2_CHANNEL> m_channel;
};

class Command
{
  std::string m_command;
  char m_reply_delimiter;

 public:
  explicit Command(std::string const& command, char delimiter = '\r')
      : m_command(boost::trim_copy(command)), m_reply_delimiter(delimiter)
  {
    m_command += std::string("; echo -en '") + m_reply_delimiter + "'\n";
  }
  std::string str() const
  {
    return m_command;
  }
  char reply_delimiter() const
  {
    return m_reply_delimiter;
  }
};

inline void async_connect(
    Shells& shells
    , boost::asio::deadline_timer::duration_type const& timeout
)
{
  std::for_each(
      shells.begin()
      , shells.end()
      , boost::bind(&Shell::async_connect, _1, timeout)
  );
}

inline void connect(
    Shells& shells
    , boost::asio::deadline_timer::duration_type const& timeout
)
{
  async_connect(shells, timeout);
  if (shells.begin() != shells.end()) {
    boost::asio::io_service& ios = (*shells.begin())->get_io_service();
    ios.reset();
    ios.run();
  }
}

inline void async_reconnect(
    Shells& shells
    , boost::asio::deadline_timer::duration_type const& timeout
)
{
  for (Shells::iterator b = shells.begin(), e = shells.end(); b != e; ++b) {
    ShellPtr s = *b;
    if (!s->is_connected()) {
      s->async_connect(timeout);
    }
  }
}

inline void reconnect(
    Shells& shells
    , boost::asio::deadline_timer::duration_type const& timeout
)
{
  async_reconnect(shells, timeout);
  if (shells.begin() != shells.end()) {
    boost::asio::io_service& ios = (*shells.begin())->get_io_service();
    ios.reset();
    ios.run();
  }
}

inline void set_log_verbosity(Shells& shells, int v)
{
  std::for_each(
      shells.begin()
      , shells.end()
      , boost::bind(&Shell::log_verbosity, _1, v)
  );
}

inline void async_exec(
    Shells& shells
    , Command const& command
    , boost::asio::deadline_timer::duration_type const& timeout
)
{
  std::for_each(
      shells.begin()
      , shells.end()
      , boost::bind(&Shell::async_exec, _1, command, timeout)
  );
}

inline void exec(
    Shells& shells
    , Command const& command
    , boost::asio::deadline_timer::duration_type const& timeout
)
{
  async_exec(shells, command, timeout);
  if (shells.begin() != shells.end()) {
    boost::asio::io_service& ios = (*shells.begin())->get_io_service();
    ios.reset();
    ios.run();
  }
}

inline void disconnect(
    Shells& shells
)
{
  std::for_each(
      shells.begin()
      , shells.end()
      , boost::bind(&Shell::disconnect, _1)
  );
  // even if the disconnect is not async, the operation cancels asio objects so
  // it may be a good idea to run possible cancelled handlers
  if (shells.begin() != shells.end()) {
    boost::asio::io_service& ios = (*shells.begin())->get_io_service();
    ios.reset();
    ios.run();
  }
}

}

#endif
