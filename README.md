# README #

SshShell is my attempt to have a simple C++ library to execute remote
commands on a number of hosts in a non-blocking way. It's based on top
of [Boost.Asio](http://think-async.com/) and
[libssh2](http://www.libssh2.org/). It relies on asio for driving the
network communication and on libssh2 to do the actual read and
writes. SshShell was first implemented on top of
[libssh](https://www.libssh.org/), but I was not able to find a way to
tell libssh just to report if it needs to read or write some more data
from/to the network.

Once open, the SSH channel is kept open after a command is submitted,
so multiple commands can be issued without reconnecting.

The library has many limitations, most notably:

1. only public-key authentication is available
1. the identity file is hard-coded to be id_rsa and is taken from the
   ssh directory passed during the construction of the shell
1. there is no verification of the host
1. there is no pty, X11, forwarding, ... support
1. there is only one channel per session
1. many others that I don't even know about

Not that (some of) the above issues are difficult to address, it's
just that the current implementation is sufficient for my current
uses.

### Hello, World! ###

* The build system is based on CMake, so a simple

```
#!sh
git clone https://giacomini@bitbucket.org/giacomini/sshshell.git
cd sshshell
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=<installation-path> .. && make install
```

will build and install the library. You'll find a libsshshell.so in
<installation-path>/lib and a ssh_shell.hpp in
<installation-path>/include.

If libssh2 is installed in a non-default location, just pass
-DCMAKE_PREFIX_PATH=<libssh-installation-path> to the cmake
command. Similarly for boost.

The build procedure also builds an executable (called _ex_) that you
can use as a starting point. See Testing below on how to set up a
testing environment.

* The available functionality is summarized in the following code

```
#!c++
asio::io_service ios;
ShellPtr shell = Shell::create(ios, ssh_dir, user, host));

asio::deadline_timer::duration_type connect_timeout = boost::posix_time::seconds(2);
shell->async_connect(connect_timeout);
ios.run();
std::cout << shell->public_key_hash() << '\n';

asio::deadline_timer::duration_type submit_timeout = boost::posix_time::seconds(2);
shell->async_exec(Command("echo 'Hello, World!'"), submit_timeout);
ios.reset(); ios.run();
std::cout << shell->release_reply() << '\n';

shell->disconnect();
```

### Testing ###

The repository includes Dockerfile's and a docker-compose.yml to
bootstrap an infrastructure with a number of containers running sshd,
all logging to a syslog running in another container. The logs can
then be checked from still another container (started manually) that
mounts the volumes from the syslog container.

```
#!sh
$ cd sshshell/docker
$ docker-compose up -d
$ docker-compose ps
     Name                Command           State   Ports  
---------------------------------------------------------
docker_sshd_1     /usr/sbin/sshd -D        Up      22/tcp 
docker_syslog_1   /bin/sh -c rsyslogd -n   Up             
$ docker inspect -f {{.NetworkSettings.IPAddress}} docker_sshd_1
172.17.0.13
$ docker run -it --rm --volumes-from docker_varlog_1 ubuntu /bin/bash
root@e15aadee4300:/# tail -f /var/log/auth.log  
...
```

The IP address of the sshd container can then be passed on the
standard input to the built executable:

```
#!sh
$ cd sshshell/build
$ mkdir ssh_dir
$ cp ../docker/guest_rsa ssh_dir/id_rsa
$ cp ../docker/guest_rsa.pub ssh_dir/id_rsa.pub
$ echo 172.17.0.13 | ./ex
...
```

You should see some logging in the auth.log mentioned above.

### Design and Implementation ###

As said above, the design is based on asio for the asynchronous
notification of network events (through the use of null_buffers) and
on libssh2 for the actual reading from and writing to the
network. A libssh2 function systematically returns an EAGAIN status if
the underlying network communication would block and another libssh2
function can be used to determine if another read or another write is
needed.

When a timeout expires, either for the connection or for a command
execution, the implementation disconnects from the remote host and a
reconnection is needed before issuing another command. When a
disconnection occurs, the output of the command, probably partial if
any, should be considered as not available.

A tricky aspect concerns the output of the remotely-executed command,
i.e. the reply: how to tell if the output is finished? since the shell
is not interactive and there is no terminal attached, there is no
prompt that one could use to stop the reading. Following an idea
suggested on the
[libssh mailing list](https://www.libssh.org/archive/libssh/2010-02/0000034.html),
the reply must terminate with a pre-agreed character. The use of only
one character, instead of a pattern for example, is to keep the
implementation simple, avoiding for example the management of partial
matches. The Command class helps to manage the command string and the
use of the delimiter (which defaults to the '\r' character).

A few types and functions are available for collective operations, so
that it becomes easier to do thinks like

```
#!c++
Shells shells;
shells.push_back(Shell::create(ios, ssh_dir, user, host));
...
connect(shells, connect_timeout);
exec(shells, Command("echo 'Hello, World!'"), submit_timeout);
disconnect(shells);
```

The implementation is written for the moment in C++03.

Enjoy!
