#include "ssh_shell.hpp"
#include <boost/asio.hpp>
#include <iostream>
#include <boost/filesystem/path.hpp>
#include <boost/thread.hpp>

namespace asio = boost::asio;
namespace fs = boost::filesystem;

using namespace ssh;

void print_pubkey_hash(std::ostream& os, ShellPtr const& s)
{
  os << s->host() << ": ";
  if (s->is_connected()) {
    os << s->public_key_hash() << std::endl;
  } else {
    os << "not connected\n";
  }
}

void print_pubkey_hash(Shells const & shells)
{
  std::for_each(
      shells.begin()
      , shells.end()
      , boost::bind(print_pubkey_hash, boost::ref(std::cerr), _1)
  );
}

void print_reply(std::ostream& os, ShellPtr const& s)
{
  os << s->host() << ": ";
  if (s->is_connected()) {
     os << s->release_reply() << std::flush;
  } else {
    os << "not connected\n";
  }
}

void print_reply(Shells const & shells)
{
  std::for_each(
      shells.begin()
      , shells.end()
      , boost::bind(print_reply, boost::ref(std::cerr), _1)
  );
}

int main()
{
  try {

    asio::io_service ios;
    Shells shells;
    std::string const user("guest");
    fs::path const ssh_dir = fs::current_path()/"ssh_dir";
    assert(ssh_dir.is_absolute());

    for (std::string host; std::getline(std::cin, host); ) {
      shells.push_back(Shell::create(ios, ssh_dir, user, host));
    }

    // LIBSSH2_TRACE_SOCKET    Socket low-level debugging
    // LIBSSH2_TRACE_TRANS     Transport layer debugging
    // LIBSSH2_TRACE_KEX       Key exchange debugging
    // LIBSSH2_TRACE_AUTH      Authentication debugging
    // LIBSSH2_TRACE_CONN      Connection layer debugging
    // LIBSSH2_TRACE_SCP       SCP debugging
    // LIBSSH2_TRACE_SFTP      SFTP debugging
    // LIBSSH2_TRACE_ERROR     Error debugging
    // LIBSSH2_TRACE_PUBLICKEY Public Key debugging
  
    // set_log_verbosity(shells, LIBSSH2_TRACE_SOCKET );

    asio::deadline_timer::duration_type connect_timeout
        = boost::posix_time::seconds(3);
    asio::deadline_timer::duration_type submit_timeout
        = boost::posix_time::seconds(2);

    connect(shells, connect_timeout);
    print_pubkey_hash(shells);

    exec(shells, Command("sleep 10 && pwd"), submit_timeout);
    print_reply(shells);

    boost::this_thread::sleep_for(boost::chrono::seconds(5));

    reconnect(shells, connect_timeout);

    exec(
        shells
        , Command("for i in `seq 1 10`; do hostname; done")
        , submit_timeout
    );
    print_reply(shells);

    disconnect(shells);

  } catch (std::exception const& e) {
    std::cerr << "Exception: " << e.what() << '\n';
    return EXIT_FAILURE;
  } catch (...) {
    std::cerr << "Unknown exception\n";
    return EXIT_FAILURE;
  }
}
